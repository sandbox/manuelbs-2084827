<?php
/**
 * @file
 * erpal_products.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function erpal_products_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'products_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Products';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '<None>';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_supplier_ref_node']['id'] = 'reverse_field_supplier_ref_node';
  $handler->display->display_options['relationships']['reverse_field_supplier_ref_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_supplier_ref_node']['field'] = 'reverse_field_supplier_ref_node';
  /* Field: Content: Article Identifier */
  $handler->display->display_options['fields']['field_article_identifier']['id'] = 'field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['table'] = 'field_data_field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['field'] = 'field_article_identifier';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'erpal_product' => 'erpal_product',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'products-view';

  /* Display: Products pane */
  $handler = $view->new_display('panel_pane', 'Products pane', 'panel_pane_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_article_identifier' => 'field_article_identifier',
    'title' => 'title',
    'field_product_type_tags' => 'field_product_type_tags',
    'field_price_per_unit' => 'field_price_per_unit',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_article_identifier' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_product_type_tags' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_price_per_unit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Price (field_product_price) */
  $handler->display->display_options['relationships']['field_product_price_value']['id'] = 'field_product_price_value';
  $handler->display->display_options['relationships']['field_product_price_value']['table'] = 'field_data_field_product_price';
  $handler->display->display_options['relationships']['field_product_price_value']['field'] = 'field_product_price_value';
  $handler->display->display_options['relationships']['field_product_price_value']['delta'] = '-1';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Article Identifier */
  $handler->display->display_options['fields']['field_article_identifier']['id'] = 'field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['table'] = 'field_data_field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['field'] = 'field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['label'] = '';
  $handler->display->display_options['fields']['field_article_identifier']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Product';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Product Type */
  $handler->display->display_options['fields']['field_product_type_tags']['id'] = 'field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['table'] = 'field_data_field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['field'] = 'field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['delta_offset'] = '0';
  /* Field: Field collection item: Price per unit */
  $handler->display->display_options['fields']['field_price_per_unit']['id'] = 'field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['table'] = 'field_data_field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['field'] = 'field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['relationship'] = 'field_product_price_value';
  $handler->display->display_options['fields']['field_price_per_unit']['alter']['text'] = '<div style="text-align:right;">[field_price_per_unit]</div>';
  $handler->display->display_options['fields']['field_price_per_unit']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_products_currency']['id'] = 'field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['table'] = 'field_data_field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['field'] = 'field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['relationship'] = 'field_product_price_value';
  /* Field: Field collection item: Minimum amount */
  $handler->display->display_options['fields']['field_products_minimum_amount']['id'] = 'field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['table'] = 'field_data_field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['field'] = 'field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['relationship'] = 'field_product_price_value';
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_products_validity']['id'] = 'field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['table'] = 'field_data_field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['field'] = 'field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['relationship'] = 'field_product_price_value';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'erpal_product' => 'erpal_product',
  );
  /* Filter criterion: Content: Status (field_status_is_active) */
  $handler->display->display_options['filters']['field_status_is_active_value']['id'] = 'field_status_is_active_value';
  $handler->display->display_options['filters']['field_status_is_active_value']['table'] = 'field_data_field_status_is_active';
  $handler->display->display_options['filters']['field_status_is_active_value']['field'] = 'field_status_is_active_value';
  $handler->display->display_options['filters']['field_status_is_active_value']['value'] = array(
    1 => '1',
  );

  /* Display: summary by product */
  $handler = $view->new_display('panel_pane', 'summary by product', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_article_identifier' => 'field_article_identifier',
    'field_status_is_active' => 'field_status_is_active',
    'field_product_dimensions' => 'field_product_dimensions',
    'field_product_weight' => 'field_product_weight',
    'field_acquisition_method' => 'field_acquisition_method',
    'field_supplier_ref' => 'field_supplier_ref',
    'field_product_type_tags' => 'field_product_type_tags',
  );
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Article Identifier */
  $handler->display->display_options['fields']['field_article_identifier']['id'] = 'field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['table'] = 'field_data_field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['field'] = 'field_article_identifier';
  $handler->display->display_options['fields']['field_article_identifier']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_article_identifier']['alter']['text'] = '[field_article_identifier]<br />';
  $handler->display->display_options['fields']['field_article_identifier']['element_label_type'] = 'strong';
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_status_is_active']['id'] = 'field_status_is_active';
  $handler->display->display_options['fields']['field_status_is_active']['table'] = 'field_data_field_status_is_active';
  $handler->display->display_options['fields']['field_status_is_active']['field'] = 'field_status_is_active';
  $handler->display->display_options['fields']['field_status_is_active']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_status_is_active']['alter']['text'] = '[field_status_is_active]<br />';
  $handler->display->display_options['fields']['field_status_is_active']['element_label_type'] = 'strong';
  /* Field: Content: Dimensions */
  $handler->display->display_options['fields']['field_product_dimensions']['id'] = 'field_product_dimensions';
  $handler->display->display_options['fields']['field_product_dimensions']['table'] = 'field_data_field_product_dimensions';
  $handler->display->display_options['fields']['field_product_dimensions']['field'] = 'field_product_dimensions';
  $handler->display->display_options['fields']['field_product_dimensions']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_product_dimensions']['alter']['text'] = '[field_product_dimensions]<br />';
  $handler->display->display_options['fields']['field_product_dimensions']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_product_dimensions']['click_sort_column'] = 'length';
  /* Field: Content: Weight */
  $handler->display->display_options['fields']['field_product_weight']['id'] = 'field_product_weight';
  $handler->display->display_options['fields']['field_product_weight']['table'] = 'field_data_field_product_weight';
  $handler->display->display_options['fields']['field_product_weight']['field'] = 'field_product_weight';
  $handler->display->display_options['fields']['field_product_weight']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_product_weight']['alter']['text'] = '[field_product_weight]<br />';
  $handler->display->display_options['fields']['field_product_weight']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_product_weight']['click_sort_column'] = 'weight';
  /* Field: Content: Acquisition Method */
  $handler->display->display_options['fields']['field_acquisition_method']['id'] = 'field_acquisition_method';
  $handler->display->display_options['fields']['field_acquisition_method']['table'] = 'field_data_field_acquisition_method';
  $handler->display->display_options['fields']['field_acquisition_method']['field'] = 'field_acquisition_method';
  $handler->display->display_options['fields']['field_acquisition_method']['alter']['text'] = '[field_acquisition_method], ';
  $handler->display->display_options['fields']['field_acquisition_method']['element_label_type'] = 'strong';
  /* Field: Content: Supplier */
  $handler->display->display_options['fields']['field_supplier_ref']['id'] = 'field_supplier_ref';
  $handler->display->display_options['fields']['field_supplier_ref']['table'] = 'field_data_field_supplier_ref';
  $handler->display->display_options['fields']['field_supplier_ref']['field'] = 'field_supplier_ref';
  $handler->display->display_options['fields']['field_supplier_ref']['label'] = '/ Supplier';
  $handler->display->display_options['fields']['field_supplier_ref']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_supplier_ref']['type'] = 'entityreference_typed_label';
  $handler->display->display_options['fields']['field_supplier_ref']['settings'] = array(
    'link' => 1,
  );
  /* Field: Content: Product Type */
  $handler->display->display_options['fields']['field_product_type_tags']['id'] = 'field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['table'] = 'field_data_field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['field'] = 'field_product_type_tags';
  $handler->display->display_options['fields']['field_product_type_tags']['label'] = 'Product Type(s)';
  $handler->display->display_options['fields']['field_product_type_tags']['alter']['text'] = '[field_product_type_tags]<br />';
  $handler->display->display_options['fields']['field_product_type_tags']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_product_type_tags']['element_wrapper_type'] = 'p';
  $handler->display->display_options['fields']['field_product_type_tags']['delta_offset'] = '0';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Description';
  $handler->display->display_options['fields']['body']['alter']['text'] = '<strong>Description:</strong><br />
[body-summary]';
  $handler->display->display_options['fields']['body']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'erpal_product' => 'erpal_product',
  );

  /* Display: details by product */
  $handler = $view->new_display('panel_pane', 'details by product', 'panel_pane_3');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '<br />[body]';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;

  /* Display: files by product */
  $handler = $view->new_display('panel_pane', 'files by product', 'panel_pane_4');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field: Files */
  $handler->display->display_options['fields']['field_asset_files']['id'] = 'field_asset_files';
  $handler->display->display_options['fields']['field_asset_files']['table'] = 'field_data_field_asset_files';
  $handler->display->display_options['fields']['field_asset_files']['field'] = 'field_asset_files';
  $handler->display->display_options['fields']['field_asset_files']['label'] = '';
  $handler->display->display_options['fields']['field_asset_files']['alter']['text'] = '<img>[field_asset_files]</img>';
  $handler->display->display_options['fields']['field_asset_files']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_asset_files']['type'] = 'entityreference_file_node';
  $handler->display->display_options['fields']['field_asset_files']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_asset_files']['delta_offset'] = '0';

  /* Display: prices by product */
  $handler = $view->new_display('panel_pane', 'prices by product', 'panel_pane_5');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_products_currency' => 'field_products_currency',
    'field_price_per_unit' => 'field_price_per_unit',
    'field_products_minimum_amount' => 'field_products_minimum_amount',
    'field_products_validity' => 'field_products_validity',
  );
  $handler->display->display_options['style_options']['default'] = 'field_price_per_unit';
  $handler->display->display_options['style_options']['info'] = array(
    'field_products_currency' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_price_per_unit' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_products_minimum_amount' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_products_validity' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Price (field_product_price) */
  $handler->display->display_options['relationships']['field_product_price_value']['id'] = 'field_product_price_value';
  $handler->display->display_options['relationships']['field_product_price_value']['table'] = 'field_data_field_product_price';
  $handler->display->display_options['relationships']['field_product_price_value']['field'] = 'field_product_price_value';
  $handler->display->display_options['relationships']['field_product_price_value']['delta'] = '-1';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_products_currency']['id'] = 'field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['table'] = 'field_data_field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['field'] = 'field_products_currency';
  $handler->display->display_options['fields']['field_products_currency']['relationship'] = 'field_product_price_value';
  $handler->display->display_options['fields']['field_products_currency']['exclude'] = TRUE;
  /* Field: Field collection item: Price per unit */
  $handler->display->display_options['fields']['field_price_per_unit']['id'] = 'field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['table'] = 'field_data_field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['field'] = 'field_price_per_unit';
  $handler->display->display_options['fields']['field_price_per_unit']['relationship'] = 'field_product_price_value';
  $handler->display->display_options['fields']['field_price_per_unit']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_price_per_unit']['alter']['text'] = '[field_price_per_unit] [field_products_currency]';
  $handler->display->display_options['fields']['field_price_per_unit']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Field collection item: Minimum amount */
  $handler->display->display_options['fields']['field_products_minimum_amount']['id'] = 'field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['table'] = 'field_data_field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['field'] = 'field_products_minimum_amount';
  $handler->display->display_options['fields']['field_products_minimum_amount']['relationship'] = 'field_product_price_value';
  $handler->display->display_options['fields']['field_products_minimum_amount']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  /* Field: Broken/missing handler */
  $handler->display->display_options['fields']['field_products_validity']['id'] = 'field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['table'] = 'field_data_field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['field'] = 'field_products_validity';
  $handler->display->display_options['fields']['field_products_validity']['relationship'] = 'field_product_price_value';
  $handler->display->display_options['fields']['field_products_validity']['label'] = 'Validity';
  $export['products_view'] = $view;

  return $export;
}
