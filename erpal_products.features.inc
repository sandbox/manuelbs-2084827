<?php
/**
 * @file
 * erpal_products.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function erpal_products_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function erpal_products_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function erpal_products_node_info() {
  $items = array(
    'erpal_product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('This is the basic content type for products in ERPAL'),
      'has_title' => '1',
      'title_label' => t('Product Name'),
      'help' => '',
    ),
  );
  return $items;
}
