<?php
/**
 * @file
 * erpal_products.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function erpal_products_taxonomy_default_vocabularies() {
  return array(
    'product_types' => array(
      'name' => 'Product types',
      'machine_name' => 'product_types',
      'description' => 'Taxonomy for product categories',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
