<?php
/**
 * @file
 * erpal_products.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function erpal_products_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_div_measurements|node|erpal_product|default';
  $field_group->group_name = 'group_div_measurements';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'erpal_product';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_measurements';
  $field_group->data = array(
    'label' => 'HWD',
    'weight' => '15',
    'children' => array(
      0 => 'field_measurements_depth',
      1 => 'field_measurements_height',
      2 => 'field_measurements_width',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'HWD',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_div_measurements|node|erpal_product|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_measurements|node|erpal_product|default';
  $field_group->group_name = 'group_measurements';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'erpal_product';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Measurements',
    'weight' => '5',
    'children' => array(
      0 => 'field_measurements_weight',
      1 => 'group_div_measurements',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Measurements',
      'instance_settings' => array(
        'classes' => '',
        'description' => 'HxWxD',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_measurements|node|erpal_product|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_measurements|node|erpal_product|form';
  $field_group->group_name = 'group_measurements';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'erpal_product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Measurements',
    'weight' => '7',
    'children' => array(
      0 => 'field_measurements_height',
      1 => 'field_measurements_width',
      2 => 'field_measurements_depth',
      3 => 'field_measurements_weight',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Measurements',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => 'Contains weight and size information for logistic purposes if applicable.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_measurements|node|erpal_product|form'] = $field_group;

  return $export;
}
