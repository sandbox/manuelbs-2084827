<?php
/**
 * @file
 * erpal_products.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function erpal_products_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'product_content_tabs';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Product content tabs';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'products_view',
      'display' => 'panel_pane_2',
      'args' => '%1',
      'title' => 'Summary',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'products_view',
      'display' => 'panel_pane_3',
      'args' => '%1',
      'title' => 'Details',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'products_view',
      'display' => 'panel_pane_5',
      'args' => '%1',
      'title' => 'Prices',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'erpal_no_js_tabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Details');
  t('Prices');
  t('Product content tabs');
  t('Summary');

  $export['product_content_tabs'] = $quicktabs;

  return $export;
}
