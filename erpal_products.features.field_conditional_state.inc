<?php
/**
 * @file
 * erpal_products.features.field_conditional_state.inc
 */

/**
 * Implements hook_field_conditional_state_default_fields().
 */
function erpal_products_field_conditional_state_default_fields() {
  $items = array();

  $items[] = array(
    'type' => 'node',
    'bundle' => 'erpal_product',
    'field_name' => 'field_supplier_ref',
    'control_field' => 'field_acquisition_method',
    'state' => 'visible',
    'condition_type' => 'or',
    'trigger_values' => array(
      'Externally' => 'Externally',
    ),
  );

  return $items;
}
